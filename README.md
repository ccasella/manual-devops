# Using Kubernetes from GitLab without AutoDevops

## Starting point

I start with a new kubernetes cluster (in this case from GKE) without any other deployment:   

```
kubectl get deployments. --all-namespaces 
NAMESPACE     NAME                     DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
kube-system   event-exporter-v0.2.4    1         1         1            1           3m24s
kube-system   fluentd-gcp-scaler       1         1         1            1           3m11s
kube-system   heapster-v1.6.0-beta.1   1         1         1            1           3m25s
kube-system   kube-dns                 2         2         2            2           3m25s
kube-system   kube-dns-autoscaler      1         1         1            1           3m24s
kube-system   l7-default-backend       1         1         1            1           3m25s
kube-system   metrics-server-v0.3.1    1         1         1            1           3m23s
```

## Test project

In this tutorial we will deploy a simple web application based on the NGINX docker image.

## Service account

To interact with our Kubernetes cluster we are going to create a `Service Account` and the relative `Cluster Role Binding`.   
In this instance we will use just one `Service Account` with the `Cluster Admin Role` privileges, but it can be used also a `Service Account` with just the permission for the target namespace.

```
kubectl -n kube-system create serviceaccount gitlab

kubectl create clusterrolebinding gitlab \
  --clusterrole cluster-admin \
  --serviceaccount=kube-system:gitlab
```

Now we can take note of the `Secret` name for this `Service Account`:

```
kubectl get serviceaccounts -n kube-system gitlab -o yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  creationTimestamp: "2019-07-22T21:53:55Z"
  name: gitlab
  namespace: kube-system
  resourceVersion: "2451"
  selfLink: /api/v1/namespaces/kube-system/serviceaccounts/gitlab
  uid: 335b3a7b-accb-11e9-990c-42010a84011d
secrets:
- name: gitlab-token-vvnbk
```

Our secret, in this case, will be `gitlab-token-vvnbk`.

Now we retrieve the `Token` for this `Service Account` that we will use during the connection:

```
kubectl get secrets -n kube-system gitlab-token-vvnbk -o jsonpath='{.data.token}' | base64 -D; echo
eyJhbGciOiJSUzI1Nxxxxxxx[...]hdksjahflkrjeheEE
```

## Repository

Our project repo will contain the following files/directory

### Dockerfile

In our test application we are just going to change few lines in our index.html   

```
FROM nginx
COPY index.html /usr/share/nginx/html
```

### index.html
```
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server has been successfully deployed from the GitLab pipeline.</p>

<p><em>Thank you for using GitLab.</em></p>
</body>
</html>
```

### .gitlab-ci.yml

In our pipeline we are going to create two stages, in the first we create the docker image and we push it to the Registry.   
In the last we are going to use a docker image containing kubectl and 3 different environment variables to setup our Kubernetes cluster:   
* KUBE_URL - The Kubernetes API endpoint
* KUBE_TOKEN - The Token that we taken in the previous steps for our Service Account
* KUBE_NS - The namespace that we will use for our deployment    

The authentication variables CI_REGISTRY_USER and CI_REGISTRY_PASSWORD to let our Kubernetes cluster upload/download the docker image are self managed, but they can work just one time. On some cluster this can create issues, in this case substitute these variable with a `Deploy Token`.

```
stages:
- build
- deploy

Build:
  stage: build
  services:
  - docker:dind
  tags:
  - docker
  image: docker:stable
  before_script:
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
  - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME" .
  - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"

Deploy:
  stage: deploy
  image: dtzar/helm-kubectl
  only:
  - tags
  environment:
    name: production
    url: https://www.example.com
  when: manual
  script:
  - kubectl config set-cluster k8s-cluster --server="$KUBE_URL" --insecure-skip-tls-verify=true
  - kubectl config set-credentials admin --token="$KUBE_TOKEN"
  - kubectl config set-context default --cluster=k8s-cluster --user=admin --namespace=$KUBE_NS"
  - kubectl config use-context default
  - kubectl apply -f kubernetes
```

### kubernetes folder

This folder will contain all the manifest required for the kubernetes deployment. In the last command of our Deploy stage we are going to apply the whole folder.

## Release management

In this instance we are going to use tags to manage our releases. In our deployment manifest a specific tag is required and the deployment stage in our pipeline will be executed just for tags, after a manual trigger.
Our project is ready!
